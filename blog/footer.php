<footer class="footer-text">
	<p>Posted by: Francisco josé macias montes </p>
	<p> Teléono (Whatsapp) Móvil: 630 72 70 95 </p>
	<p class="footer-text p">Contact information:
		<a href="mailto:info@easyrobotics.es">
			info@easyrobotics.es</a>.</p>
</footer>
</div>
<script src="js/jquery/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<!--aqui cargo el select2 del lenguaje jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- aqui voy a cargar el ckeditor mediante el cdn-->
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
<!--aqui cargamos el jquery mockjax es decir el ajax-->
<!--<script src="js/jquery-mockjax-master/dist/jquery.mockjax.min.js"></script>-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
    crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
    crossorigin="anonymous"></script>

<script src="js/main.js"></script>
</body>

</html>