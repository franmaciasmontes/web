<?php
 //* requiren_once es lo primero que va siempre en el orden de php despues se añade todo lo demas*//
    require_once "BlogController.php";
    
    $response['validation'] = true;

    if(empty($_POST['name'])){
        
        array_push($response, array(
            'message' => "Complete con nombre",
            'id' =>"name"
        ));

        $response['validation']= false;
    }

    if(empty($_POST['lastname'])){
        
        array_push($response, array(
            'message' => "Complete con apellidos",
            'id' =>"lastname"
        ));

        $response['validation']= false;
    }

    if(empty($_POST['email'])){

        array_push($response, array(
            'message'=> "Complete con email",
            'id'=> "email"
        ));

        $response['validation']= false;

    }

    if(strlen($_POST['name']) < 2 || strlen($_POST['name']) > 21){
        
        array_push($response,array(
            'message' => "El campo nombre debe tener entre 2 y 21 carácteres",
            'id' => "name"
        ));

        $response['validation'] = false;
    }

    if(strlen($_POST['lastname']) < 2 || strlen($_POST['lastname']) > 21){

        array_push($response,array(
            'message' => "El campo apellido debe tener entre 2 y 21 carácteres",
            'id' => "lastname"
        ));

        $response['validation'] = false;
    }

    if(strlen($_POST['email']) < 5 || strlen($_POST['email']) > 40){

        array_push($response,array(
            'message' => "El campo email debe tener entre 5 y 40 carácteres",
            'id'=> "email"
        ));

        $response['validation'] = false;
    }

    if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){

        array_push($response, array(
            'message' => "Debe escribir un email válido",
            'id' => "email"
        ));

        $response['validation'] = false;
    }

    if($response['validation']){

        $blog = new blogController($_POST);    
        $response['message'] = "Bienvenido " . $blog->getblogName();
    }
   
    echo json_encode($response);
    
    
?>
