/* esta función nos valida el formulario , es importante event es como un this
<input type="submit" id="submit" name="submit" value"Enviar"/>
el event es el submit y lo pasamos a la función que activamos con el onclick 
cuando se termina de cargar el documento html se carga o ejecuta este codigo.
Solo puedo haber un document.ready en todo el documento
esta función a la libreria select2 y sirve para el boton desplegable de las comunidades autonomas hay que indicarle el name en este caso '.state'y luego la libreriar select2 
es como un alert pero dentro de la consola y no para el flujo del codigo. Checkeo para comprobar en la consola (F12) del navegador si se han metido bien todos los datos*/

$(document).ready(function () {

    if ($("textarea").length > 0) { // dices si hay mas de un ckeditor
        CKEDITOR.replace('ckeditor'); // esta función a la libreria ckeditor y sirve para transformar los textarea
    }

    $('.state').select2(); // id del select2 state pertenecientes a las comunidades escritas

});

/*Valida el formulario y le da los valores para que el usuario rellene el formulario*/
$("blog-form.php").validate({

    onfocusout: false,
    onkeyup: false,
    ignore: [],
    errorClass: "es inválido",


    rules: {

        name: {
            required: true,
            minlength: 2,
            maxlength: 18,
            lettersonly: true
        },

        lastname: {
            required: true,
            minlength: 4,
            maxlength: 40,
            lettersonly: true
        },

        email: {
            required: true,
            email: true,
            maxlength: 40,
            minlength: 5
        },
        state: {
            required: true
        },

        editor: {
            required: function () {
                CKEDITOR.instances.ckeditor.updateElement();
            }

        }
    },

    messages: {
        name: {
            required: 'El nombre es obligatorio',
            minlength: 'el mínimo de caracteres permitidos para el nombre son 2',
            maxlength: 'el máximo de caracteres permitidos para el nombres son 18',
            lettersonly: 'Sólo se aceptan letras en el nombre'
        },
        lastname: {
            required: 'El apellido es obligatorio',
            minlength: 'el mínimo de caracteres permitidos para el nombre son 4',
            maxlength: 'el máximo de caracteres permitidos para el nombres son 40',
            lettersonly: 'Sólo se aceptan letras en el nombre'
        },
        email: {
            required: 'El email es obligatorio',
            email: 'Debe introducir un correo electronico válido, como ejemplo@gmail.com',
            maxlength: 'El máximo de caracteres permitidos para el email son 40',
            minlength: 'el minimo de caracteres permitidos para el email son 5'
        },
        state: {
            required: 'true'
        },
        comentary: {
            required: 'dejenos un comentario'
        }
    }
});

$(document).on('click', '#validate', function (event) {

    event.preventDefault();

    if ($("textarea").length > 0) {
        CKEDITOR.instances.ckeditor.updateElement();
    };

    var form = "#" + $(".admin-form").attr('id'); //id del formulario 
    var url = $(form).prop("action");
    var formData = new FormData($(form)[0]); //id del formulario para comprobar que funciona en la consola

    for (var pair of formData.entries()) {
        console.log(pair[0] + ', ' + pair[1]);
    };

    $.ajax({
        type: 'post',
        url: url, //la dirreción al que llamaremos
        dataType: 'json', //es el metodo que llamamos
        data: formData, //se almacena los datos
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {

            console.log(response);

            if (response._validation) {
                $(".success").text(response.message);
                $(".admin-form").fadeOut();
                $(".success-container").removeClass("hidden").fadeOut().fadeIn();

            } else {

                $(".errors-container").removeClass("hidden");

                $.each(response._errors, function (i, item) {
                    if (item.message) {
                        $(".errors").append("<li>" + item.message + "</li>");
                    }
                });
            }
        },

        error: function (response) {
            console.log(response);
        }
    });

    return false;
});

$(document).on('click', '.del', function (event) {

    event.preventDefault();

    var id = $(this).attr('id');
    var url = $(".table").attr('action');

    alert($(this).attr('id'));
    var formData = new FormData(); // crear un formdata luego le enchufas al formdata los valores que quieras con .append y lo envias por ajax./*
    formData.append("id", $(this).attr('id'));


    /*
    $.post( url, { id: id} ).done(function( data ) {
    });
    */

    $.ajax({
        type: "POST",
        url: 'DeleteController.php', //la dirreción al que llamaremos
        data: formData, //se almacena los datos
        dataType: "json", //es el tipo de dato que espera que le devolvamos
        processData: false,
        contentType: false,
        cache: false,

        success: function (response) {

            return "este usuario se elimino"
            console.log(response);
            location.reload(true);

        },

        error: function (response){
            console.log(response);
            location.reload(true);
        }
    });

    return false;
});


$(document).on('click', '.edit', function(){//el edit coge todos los datos del formulario mediante una llamada al formdata y rellena el formulario registro.
    
    event.preventDefault();//porque esta funcionandado?

    var id = $(this).attr('id');
    var formData =  new FormData();
    formData.append("id", id);
    formData.append("title", $("#title"+id).html());//aqui mediante el append lo que hacemos es llamar al name de la tabla.
    formData.append("author", $("#author"+id).html());
    formData.append("body", $("#body"+id).html());
    
    var body = $("#body"+id).val();
   
    $.ajax({
        type: "POST",
        url: 'register.php', //la dirreción al que llamaremos
        data: formData, //se almacena los datos
        dataType: "text", //es el tipo de dato que espera que le devolvamos
        processData : false,
        contentType: false,
        cache: false,

        success: function (response) {
            $("#contenido").html(response);
            // CKEDITOR.instances.ckeditor.setData(body);
            // //$("")
        },

        error: function(response){
        }
    });
});

$(document).on('click', '.update', function(event){
    
    event.preventDefault();

    if ($("textarea").length > 0) {
        CKEDITOR.instances.ckeditor.updateElement();
    };

    var form = "#" + $(".admin-form").attr('id'); //id del formulario 
    var url = $(form).prop("action");
    var formData = new FormData($(form)[0]); //id del formulario para comprobar que funciona en la consola

    for (var pair of formData.entries()) {
        console.log(pair[0] + ', ' + pair[1]);
    };

    $.ajax({
        type: 'post',
        url: url, //la dirreción al que llamaremos
        dataType: 'json', //es el metodo que llamamos
        data: formData, //se almacena los datos
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {

            console.log(response);

            if (response._validation) {
                $(".success").text(response.message);
                $(".admin-form").fadeOut();
                $(".success-container").removeClass("hidden").fadeOut().fadeIn();

            } else {

                $(".errors-container").removeClass("hidden");

                $.each(response._errors, function (i, item) {
                    if (item.message) {
                        $(".errors").append("<li>" + item.message + "</li>");
                    }
                });
            }
        },

        error: function (response) {
            console.log(response);
        }
    });

    return false;
});


$(document).on('click', '.entry', function(){//el edit coge todos los datos del formulario mediante una llamada al formdata y rellena el formulario registro.
    
    event.preventDefault();//porque esta funcionandado?

    var id = $(this).attr('id');
    var formData =  new FormData();
    formData.append("id", id);
    formData.append("title", $("#title"+id).html());//aqui mediante el append lo que hacemos es llamar al name de la tabla.
    formData.append("author", $("#author"+id).html());
    formData.append("body", $("#body"+id).html());
    
    var body = $("#body"+id).val();
   
    $.ajax({
        type: "POST",
        url: 'entry.php', //la dirreción al que llamaremos
        data: formData, //se almacena los datos
        dataType: "text", //es el tipo de dato que espera que le devolvamos
        processData : false,
        contentType: false,
        cache: false,

        success: function (response) {
            $("#contenido").html(response);
            // CKEDITOR.instances.ckeditor.setData(body);
            // //$("")
        },

        error: function(response){
        }
    });
});