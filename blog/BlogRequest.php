<?php

    require_once('BlogController.php');
    require_once('core\Validation.php');
    
    /*$value='name' . $input_id = "name" $input_label = lo que se añade al cmapo nombre*/
    $validation = new Validation();// construir objeto en una variable, siempre arranca el constructor por si solo.
    $validation->isEmpty($_POST['title'], 'titulo', 'title');
    $validation->isEmpty($_POST['category'], 'categoría', 'category');
    $validation->isEmpty($_POST['body'], 'body', 'body');
    $validation->isEmpty($_POST['author'], 'author', 'author');
    $validation->minlength($_POST['title'],'title', "titulo",2);
    $validation->maxlength($_POST['title'],'title', "titulo",21);
    $validation->minlength($_POST['body'],'body', 'body',5);
    $validation->maxlength($_POST['body'],'body', 'body',2000);
    $validation->minlength($_POST['author'],'author', 'author',2);
    $validation->maxlength($_POST['author'],'author', 'author',21);


    //imprime  en pantalla y en donde , empaqueta el json
    if($validation->getValidation()){ 
        $blog = new BlogController($_POST);

        $response['_Validation'] = $validation->getValidation();
    
        if(isset($_POST['id'])){
            $response['message'] = $blog->updateblog($_POST); // aqui se pasa los datos de las base de datos del update. se necesita de un if o else para diferenciarlo
        }else{
            $response['message'] = $blog->createblog($_POST); // aqui se pasa a la base de datos. llamas al método y el método te devuelve la info.
        }
        
        echo json_encode($response);
        
    } else{
    echo json_encode($validation->getErrors());
}   

?> 