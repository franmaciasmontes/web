<?php
include ('header_admin.php');
require_once ("BlogController.php");
$blog_obj = new BlogController();
$blogs = $blog_obj->indexBlog();
?>

<div class="col-9 col-sm-9 col-md-6 col-lg-6 col-xl-12 hidden" id="contenido">
	<form class="admin-form" id="table" action='BlogController.php'>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>TITULO</th>
					<th>CATEGORY</th>
					<th>BODY</th>
					<th>AUTHOR</th>
					<th>ELIMINAR</th>
					<th>MODIFICAR</th>
				</tr>

				<?php foreach($blogs as $blog): ?>
					<tr>
						<td ><?= $blog['id']; ?></td>
						<td id="title<?=$blog['id']; ?>"><?= $blog['title']; ?></td>
						<td id="category<?=$blog['id']; ?>"><?= $blog['category']; ?></td>
						<td id="body<?=$blog['id']; ?>"><?= $blog['body']; ?></td> 
						<td id="author <?=$blog['id']; ?>"><?= $blog['author']; ?></td> 
						<td><button class="button del" id="<?= $blog['id'];?>">Borrar</button></td>
						<td><button class="button edit" id="<?= $blog['id'];?>">editar</button></td>
					</tr>
				<?php endforeach ?>
			</thead>
		</table>
	</form>
</div>

<?php include ('footer.php');