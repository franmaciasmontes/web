<?php
include ('header_admin.php');
require_once ("BlogController.php");
$blog_obj = new BlogController();
$blogs = $blog_obj->indexBlog();
?>

<div class="offset-1 col-9 col-sm-9 col-md-6 col-lg-12 col-xl-12 section" id="contenido">
	<form class="admin-form" id="table" action='BlogController.php'>

	<input type="hidden" value="<?= !empty($_POST) ? $_POST["id"] : '' ?>" name="id" id="id"/>	
        <table class="table">
			<thead>
                <div class="row">
                    <?php foreach($blogs as $blog): ?>                
                        <div class=" col-9 col-sm-9 col-md-6 col-lg-6 col-xl-3 blog-section">
                            <h4><?= $blog['id']; ?></h4>
                            <h3 class="title"><id="title<?=$blog['id']; ?>"><?= $blog['title']; ?></h3>
                            <h4 class="category"><id="category<?=$blog['id']; ?>"><?= $blog['category']; ?></h4>
                            <p class="hidden body"><id="body<?=$blog['id']; ?>"><?= $blog['body']; ?></p>
                            <button class="button entry" id="<?= $blog['id'];?>">ver</button>
                        </div>
                       
                    <?php endforeach ?>
                </div>
			</thead>
		</table>
	</form>
</div>

<?php include ('footer.php');
