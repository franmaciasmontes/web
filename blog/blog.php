<?php

require_once('core/Database.php');

class Blog extends Database {
 /*atributos*/ 
    protected $_title;
    protected $_category;
    protected $_body;
    protected $_author;
    
    public function __construct(){
        parent::__construct();  
    }

    public function getTitle(){
        return $this->_title;
    }
    
    public function setTitle($title){
        $this->_title = $title;
    }

    public function getCategory(){
        return $this->_category;
    }

    public function setCategory($category){
        $this->_category = $category;
    }

    public function getBody(){
        return $this->_body;
    }

    public function setBody($body){
        $this->_body = $body;
    }


    public function getAuthor(){
        return $this->_author;
    }

    public function setAuthor($author){
        $this->_author = $author;
    }

    public function indexBlog(){// la consulta que hacemos en la base de datos, desde `php si
        
        $query =  "SELECT * 
        FROM `t_blog`";

        $stmt = $this->_pdo->prepare($query);
        $stmt->execute();//stmt es declaración
        $result = $stmt->fetchAll();

        return $result;
    }

    public function showBlog($id){

        $query =  "SELECT * 
        FROM `t_blog` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    public function createBlog($blog){

        $query = "insert t_blog (title , author, category, body) 
        values (:title, :author, :category, :body)";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("title", $blog['title']);
        $stmt->bindParam("author", $blog['author']);
        $stmt->bindParam("category", $blog['category']);
        $stmt->bindParam("body", $blog['body']);
        $stmt->execute();

        $blog_id = $this->_pdo->lastInsertId();//hace la llamada a la base de datos, y dame el ultimo id que se ha añadido.

        return "Usuario añadido correctamente con el número de id " . $blog_id;
    }

    public function updateBlog($blog){

        $query = "UPDATE t_blog set
            title = :title, 
            author = :author, 
            category = :category,  
            body = :body
            WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("title", $blog['title']);
        $stmt->bindParam("author", $blog['author']);
        $stmt->bindParam("category", $blog['category']);
        $stmt->bindParam("body", $blog['body']);
        $stmt->bindParam("id", $blog['id']);
        $stmt->execute();

        return "Usuario actualizado correctamente";

    }

    public function deleteBlog($id){

        $query =  "DELETE 
        FROM `t_blog` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        
        return 'Usuario con el id ' . $id . ' ha sido eliminado.' ;

    }
    
}

?>