<?php
 // cada columna es un campo y cada fila es un registro.

require_once('core/Database.php');

class User extends Database { //Database es el objeto padre.

    /*atributos*/ 
    protected $_name;
    protected $_lastName;
    protected $_email;
    protected $_password;

    public function __construct(){
       parent::__construct();

    }   

    public function getName(){
        return $this->_name;
    }
    
    public function setName($name){
        $this->_name = $name;
    }

    public function getLastname(){
        return $this->_lastname;
    }

    public function setLastname($lastname){
        $this->_lastname = $lastname;
    }

    public function getEmail(){
        return $this->_email;
    }

    public function setEmail($email){
        $this->_email = $email;
    }

    public function getPassword(){
        return $this->_password;
    }

    public function setPassword($password){
        $this->_password = $password;
    }

    public function indexUser(){// la consulta que hacemos en la base de datos, desde `php si
        
        $query =  "SELECT * 
        FROM `t_user`";

        $stmt = $this->_pdo->prepare($query);
        $stmt->execute();//stmt es declaración
        $result = $stmt->fetchAll();

        return $result;
    }

    public function showUser($id){

        $query =  "SELECT * 
        FROM `t_user` 
        WHERE id = :id";//este id where ->

        $stmt = $this->_pdo->prepare($query);/*se prepara  la conexión a la base de datos, 
        prepara que la "query",this pdo es una propiedad de la propiedad del objeto Database, 
        se esta creando la conexión la php y la base de datos.*/
        $stmt->bindParam("id", $id); // se vincula con este bindParam"bind parametro".
        $stmt->execute();//ejecuta la consulta
        $result = $stmt->fetch(PDO::FETCH_ASSOC); // array assoicativo cada campo de la base de datos aparecera un valor. $result['name'] y saldrá el nombre que queramos.

        return $result;
    }

    public function createUser($user){

        $query = "insert t_user (name, lastname, email, password) 
        values (:name, :lastname, :email, :password)";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("name", $user['name']);//binding escribes algo aqui y automaticamente se escribe en otro sitio ,
        $stmt->bindParam("lastname", $user['lastname']);
        $stmt->bindParam("email", $user['email']);
        $stmt->bindParam("password", $user['password']);
        $stmt->execute();

        $user_id = $this->_pdo->lastInsertId();//hace la llamada a la base de datos, y dame el ultimo id que se ha añadido.

        return "Usuario añadido correctamente con el número de id " . $user_id;
    }

    public function updateUser($user){

        $query = "UPDATE t_user set
            name = :name, 
            lastname = :lastname, 
            email = :email,  
            password = :password
            WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("name", $user['name']);
        $stmt->bindParam("lastname", $user['lastname']);
        $stmt->bindParam("email", $user['email']);
        $stmt->bindParam("password", $user['password']);
        $stmt->bindParam("id", $user['id']);
        $stmt->execute();

        return "Usuario actualizado correctamente";
    
    }

    public function deleteUser($id){//borramos una fila
        $query =  "DELETE
        FROM `t_user` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return 'Usuario con el id ' . $id . ' ha sido eliminado.' ;
        
    }

}

?>
