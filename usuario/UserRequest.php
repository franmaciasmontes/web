<?php

    require_once ('UserController.php');
    require_once ('core/Validation.php');

    $validation = new Validation();// construir objeto en una variable, siempre arranca el constructor por si solo.
    $validation->isEmpty($_POST['name'],'name', "nombre");
    $validation->isEmpty($_POST['lastname'], 'lastname', "apellido");
    $validation->isEmpty($_POST['email'],'email', "email");
    $validation->minlength($_POST['name'],'name', "nombre",2);
    $validation->maxlength($_POST['name'],'name', "nombre",21);
    $validation->minlength($_POST['lastname'],'lastname', "apellidos",2);
    $validation->maxlength($_POST['lastname'],'lastname', "apellidos",21 );
    $validation->minlength($_POST['email'],'email',"email",5);
    $validation->maxlength($_POST['email'],'email',"email",40 );
    $validation->valMail($_POST['email'], 'email');

    //imprime  en pantalla y en donde , empaqueta el json
    if($validation->getValidation()){ 
        $user = new UserController();

        $response['_validation'] = $validation->getValidation();

        if(isset($_POST['id'])){
            $response['message'] = $user->updateUser($_POST); // aqui se pasa los datos de las base de datos del update. se necesita de un if o else para diferenciarlo
        }else{
            $response['message'] = $user->createUser($_POST); // aqui se pasa a la base de datos. llamas al método y el método te devuelve la info.
        }
        
        echo json_encode($response);
        
    } else{
    echo json_encode($validation->getErrors());
    }   

?> 