<?php

require_once('User.php');

class UserController {

    protected $_user;

    public function __construct(){
        $this->_user = new User();// instancia
    }

    public function indexUser(){
        return $this->_user->indexUser();
    }

    public function showUser($id){
    
        return $this->_user->showUser($id);// Return, vuelve atrás de quien lo ha llamado
    }

    public function createUser($user){// datos entran por aqui
        $user['password'] = crypt($user['password']);
        return $this->_user->createUser($user);// Salen por aqui ($user en $_POS)
    }

    public function updateUser($user){
        $user['password'] = crypt($user['password']);
        return $this->_user->updateUser($user);
    }

    public function deleteUser($id){

        return $this->_user->deleteUser($id);
    }

}

?>