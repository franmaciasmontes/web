<?php
include('header.php');
?>

<div class="errors-container hidden">
    <ul class="errors"></ul>
</div>

<div class="success-container hidden">
    <h1 class="success"></h1>
</div>

<div class="row">

    <div class=" offset-2 col-xs-8 col-md-8">

        <form class="admin-form" id="user-form" action='UserRequest.php'>

            <h1>Formulario</h1>

            <div class="form-group">
                <label for="entryName" class="">name</label>
                <input type="text" name="name" id="name" class="col-md-8 form-control"  placeholder="Escriba su nombre" >
            </div>

             <div class="form-group">
                <label for="entrylastName" class="">lastname</label>
                <input type="text" name="lastname" id="lastname" class="col-md-8 form-control"  placeholder="Escriba su apellido">
            </div>

            <div class="form-group">
                <label for="email" class="">email</label>
                <input type="email" class="col-md-8 form-control" id="email"  name="email"  placeholder="Escriba su email">
            </div>

            
            <div class="form-group">
                <label for="password" class="">password</label>
                <input type="password" class="col-md-8 form-control" id="password"  name="password"  placeholder="Escriba su contraseña">
            </div>

            <div class="form-group">
                <select>
                    <option value="Ar">Aragón</option>
                    <option value="Ca">Cataluña</option>
                    <option value="Ib">Islas baleares</option>
                    <option value="Pv">País Vasco</option>
                </select>
            </div>

            <!-- el input submit sirve para validar el forumulrios-->
            <button  id="index" class="userAction" value="single/multiple" formaction="table.php">Return</button>
            <input type="submit" id="validate" class="col-md-8 btn btn-primary validate" value="enviar">
            <br/> 

        </form>

    </div>

</div>

<?php 
include('footer.php');
?>
