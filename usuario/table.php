<?php
include ('header_admin.php');
require_once ("UserController.php");
$user_obj = new UserController();
$users = $user_obj->indexUser();
?>

<div class="col-9 col-sm-9 col-md-6 col-lg-6 col-xl-12 hidden" id="contenido">
	<form class="admin-form" id="table" action='UserController.php'>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>NOMBRE</th>
					<th>APELLIDO</th>
					<th>EMAIL</th>
					<th>PASSWORD</th>
					<th>ELIMINAR</th>
					<th>MODIFICAR</th>
				</tr>

				<?php foreach($users as $user): ?>
					<tr>
						<td ><?=$user['id']; ?></td>
						<td id="name<?=$user['id']; ?>"><?= $user['name']; ?></td> <!--aqui le damos el valor del campo id y asi a todos los campos -->
						<td id="lastname<?=$user['id']; ?>"><?= $user['lastname']; ?></td>
						<td id="email<?=$user['id']; ?>"><?= $user['email']; ?></td>
						<td id="password<?=$user['id']; ?>"><?= $user['password']; ?></td> 
						<td><button class="button del" id="<?= $user['id'];?>">Borrar</button></td> 
						<td><button class="button edit" id="<?= $user['id'];?>">Editar</button></td> <!--con el boton edita rellenamos el formulario con los datos de toda la tabla.-->
					</tr>
				<?php endforeach ?>
			</thead>
		</table>
	</form>	
</div>

<?php include ('footer.php'); ?>