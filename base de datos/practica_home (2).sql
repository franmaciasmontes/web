-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-06-2018 a las 12:56:55
-- Versión del servidor: 5.6.20-log
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `practica_home`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_blog`
--

CREATE TABLE IF NOT EXISTS `t_blog` (
`id` int(11) NOT NULL,
  `title` varchar(21) COLLATE utf8_bin NOT NULL,
  `category` enum('tecnologia','ciencias','curiosidades','opinion') COLLATE utf8_bin NOT NULL,
  `body` text COLLATE utf8_bin NOT NULL,
  `author` int(21) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` date NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=24 ;

--
-- Volcado de datos para la tabla `t_blog`
--

INSERT INTO `t_blog` (`id`, `title`, `category`, `body`, `author`, `create_at`, `update_at`, `active`) VALUES
(17, 'wertwertwertwer', 'tecnologia', '<p>wertwertwertwertwertwert</p>\r\n', 0, '2018-06-19 15:34:21', '0000-00-00', 1),
(19, 'reyy', 'tecnologia', '<p>ddddddddd</p>\r\n', 0, '2018-06-21 14:07:11', '0000-00-00', 1),
(20, 'frfrfr', 'tecnologia', '<p>iiiiiiiiiiiiiiiiiiiiiiii</p>\r\n', 0, '2018-06-21 14:07:31', '0000-00-00', 1),
(21, 'frfrfr', 'tecnologia', '<p>iiiiiiiiiiiiiiiiiiiiiiii</p>\r\n', 0, '2018-06-21 14:08:28', '0000-00-00', 1),
(23, 'retwertrwet', 'tecnologia', '<p>wertwertwertwertwert</p>\r\n', 0, '2018-06-21 14:34:46', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_slider`
--

CREATE TABLE IF NOT EXISTS `t_slider` (
`id` int(11) NOT NULL,
  `img_url` varchar(40) COLLATE utf8_bin NOT NULL,
  `link` varchar(40) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin NOT NULL,
  `title` varchar(40) COLLATE utf8_bin NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` date NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=29 ;

--
-- Volcado de datos para la tabla `t_slider`
--

INSERT INTO `t_slider` (`id`, `img_url`, `link`, `description`, `title`, `create_at`, `update_at`, `active`) VALUES
(13, 'practica_home/img/slider/16697-200.png', 'marcianito.com', '<p>qwerqwerqwerwqer</p>\r\n', 'marcianito', '2018-06-19 09:31:36', '0000-00-00', 1),
(14, 'practica_home/img/slider/16697-200.png', 'mundo', '<p>ejemplo</p>\r\n', 'hola', '2018-06-22 07:35:33', '0000-00-00', 1),
(25, 'practica_home/img/slider/2018-06-12.png', 'amigo', '<p>hello amigo</p>\r\n', 'hello', '2018-06-22 08:53:59', '0000-00-00', 1),
(26, 'practica_home/img/slider/ok.png', 'amigo', '<p>hello amigo</p>\r\n', 'hello', '2018-06-22 08:54:41', '0000-00-00', 1),
(27, 'practica_home/img/slider/16697-200.png', 'amigo', '<p>hello amigo</p>\r\n', 'RTEEo', '2018-06-22 08:56:09', '0000-00-00', 1),
(28, 'practica_home/img/slider/16697-200.png', 'IOIOIO', '<p>IOIOIO</p>\r\n', 'IOIO', '2018-06-22 08:56:26', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
`id` int(11) NOT NULL,
  `name` varchar(21) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(21) COLLATE utf8_bin NOT NULL,
  `email` varchar(40) COLLATE utf8_bin NOT NULL,
  `password` varchar(60) COLLATE utf8_bin NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_at` date NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=77 ;

--
-- Volcado de datos para la tabla `t_user`
--

INSERT INTO `t_user` (`id`, `name`, `lastname`, `email`, `password`, `create_at`, `update_at`, `active`) VALUES
(72, 'eryyy', 'er', 'hjqwehrjqwe@kjerewt.com', '$1$m6..1i3.$5pZB8lcSxtoLBm3yBtNO4.', '2018-06-22 11:54:08', '0000-00-00', 1),
(74, 'noelia', 'molina', 'qwrqwerqwer@gmail.com', '$1$zy1.AO..$dylx81SlHHJYeuHaAqwXO.', '2018-06-21 12:09:24', '0000-00-00', 1),
(75, 'rturtu', 'rturturturtu', 'rtuuuuurturturt@gmail.com', '$1$JS..e03.$astwaNOcQ5ZxgFZceXpw/.', '2018-06-22 07:41:57', '0000-00-00', 1),
(76, 'fran', 'macias', 'fran@gmail.com', '$1$F1/.qQ..$VxPWbLfelcXinrs3gqRmZ0', '2018-06-22 07:43:40', '0000-00-00', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `t_blog`
--
ALTER TABLE `t_blog`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Indices de la tabla `t_slider`
--
ALTER TABLE `t_slider`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `t_user`
--
ALTER TABLE `t_user`
 ADD PRIMARY KEY (`id`), ADD KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `t_blog`
--
ALTER TABLE `t_blog`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `t_slider`
--
ALTER TABLE `t_slider`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `t_user`
--
ALTER TABLE `t_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=77;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
