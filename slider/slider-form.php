<?php 
include('header.php');
?>

<div class="row">

<div class=" offset-2 col-xs-8 col-md-8">

    <form class="admin-form" id='slider-form' action='SliderRequest.php'>

        <h1>slider</h1>

        <div class="form-group">
            <label for="imagen">imagen</label>
            <input type="file" name="img_url" id="img"/>
        </div>

        <div class="form-group">
            <label for="titulo" class="title">titulo</label>
            <input type="text" name="title" id="title" class="col-md-8 form-control" placeholder="titulo">
        </div>

        <div class="form-group">
            <label for="link" class="link">link</label>
            <input type="text" name="link" id="link" class="col-md-8 form-control" placeholder="link">
            </select>
        </div>

        <label for="description" class="description">
            <textarea name="description" id="ckeditor" rows="10" cols="80"></textarea>
        </label>

        <!-- el input submit sirve para validar el forumulrios-->
        <button  id="index" class="userAction" value="single/multiple" formaction="table.php">Return</button>
        <input type="submit" id="validate" class="col-md-8 btn btn-primary validate" value="enviar">
        <br/>

    </form>
</div>
</div>

<?php
include('footer.php');
?>