<?php

    class validation{

        protected $_validation;
        protected $_errors;

        public function __construct(){
            $this->_validation = true;
            $this->_errors = array();   
        }

        public function getValidation(){
            return $this->_validation;
        }

        public function getErrors(){
            return get_object_vars($this);
        }

        public function isEmpty($value, $input_label, $input_id){   
            if(empty($value)){               
                array_push($this->_errors, array(
                    'message' => "no has rellenado el campo ".  $input_label,
                    'id' => $input_id
                ));

                $this->_validation = false;
            }

        }

        public function fileType($file, $input_label, $allowed_extensions){

            $extension = pathinfo($file['name'], PATHINFO_EXTENSION);

            if(!in_array($extension, $allowed_extensions)){  

                array_push($this->_errors, array(
                    'message' => "el formato no es valido",
                    'id' => "img"
                ));

                $this->_validation = false;
            }

        }

        
        public function imgSize($value, $input_label, $input_id){   
            if($value['img']['img']>2000000){               
                array_push($this->_errors, array(
                    'message' => "has superado el tamaño máximo 2MB",
                    'id' => 'img'
                ));

                $this->_validation = false;
            }

        }
        public function minlength($value, $input_id, $input_label, $min_length){

            if(strlen($value) < $min_length){ 

                array_push($this->_errors, array(
                    'message' => "debe tener entre 2 y 21 carácteres " . $input_label,
                    'id' => $input_id
                ));
                $this->_validation = false;
            }
        }


        public function maxlength($value, $input_id, $input_label, $maxlength){

            if(strlen($value) > $maxlength){ 

                array_push($this->_errors, array(
                    'message' => "el campo " . $input_label .  " debe tener como máximo " . $maxlength . " carácteres",
                    'id' => $input_id
                ));

                $this->_validation = false;
            }
        }

        public function imgEmpty($value, $input_label, $input_id){   
            if(empty($value)){               
                array_push($this->_errors, array(
                    'message' => "no has selecionado ninguna imagen",
                    'id' => 'img'
                ));

                $this->_validation = false;
            }

        }

        // public function fileFormat($value, $input_id, $input_label, $formats){
        //     $pass = false;
        //     $finfo = finfo_open(FILEINFO_MIME_TYPE);
        //     $fileFormat = finfo_file($finfo, $value);

        //     foreach ($formats as $format){

        //         if ($fileFormat == $format){
        //             $pass = true;
        //         }

        //     } 

        //         
        //         ));

        //         $this->_validation = false;
        //     }
        // }

        public function valMail($value, $input_id){

            if(!filter_var($value, FILTER_VALIDATE_EMAIL)){
            
                array_push($this->_errors, array(
                    'message' => "El correo no tiene un formato válido ",
                    'id' => 'input_id'
                ));

                $this->_validation = false;
            }
        }
    }
?>