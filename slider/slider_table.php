<?php
include ('header_admin.php');
require_once ("sliderController.php");
$slider_obj = new sliderController();
$sliders = $slider_obj->indexslider();
?>


<div class="col-9 col-sm-9 col-md-6 col-lg-6 col-xl-12" id="contenido">
	<form class="admin-form" id="table" action='SliderController.php'>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>IMAGEN</th>
					<th>TITULO</th>
					<th>ENLACE</th>
					<th>DESCRIPTION</th>
					<th>ELIMINAR</th>
					<th>MODIFICAR</th>
				</tr>

				<?php foreach($sliders as $slider): ?>
					<tr>
						<td><?= $slider['id']; ?></td>
						<td id="img_url<?=$slider['id']; ?>"><?= $slider['img_url']; ?></td>
						<td id="title<?=$slider['id']; ?>"><?= $slider['title']; ?></td>
						<td id="link<?=$slider['id']; ?>"><?= $slider['link']; ?></td>
						<td id="description<?=$slider['id']; ?>"><?= $slider['description']; ?></td> 
						<td><button class="button del" id="<?= $slider['id'];?>">Borrar</button></td>   
						<td><button class="button edit" id="<?= $slider['id'];?>">Editar</button></td>
				<?php endforeach ?>
			</thead>
		</table>
    </form>
</div>

<?php include ('footer.php'); ?>