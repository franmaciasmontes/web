<?php

require_once('core/Database.php');

class Slider extends Database{
 /*atributos*/ 
    protected $_file;
    protected $_title;
    protected $_link;
    protected $_description;
    

    public function __construct(){
        parent::__construct();  
    }

    public function getTitle(){
        return $this->_title;
    }
    
    public function setTitle($title){
        $this->_title = $title;
    }

    public function getFile(){
        return $this->_file;
    }

    public function setFile($file){
        $this->_file = $file;
    }

    public function getLink(){
        return $this->_link;
    }

    public function setLink($link){
        $this->_link = $link;
    }


    public function getDescription(){
        return $this->_description;
    }

    public function setDescription($description){
        $this->_description = $description;
    }

    public function indexSlider(){// la consulta que hacemos en la base de datos, desde `php si
        
        $query =  "SELECT * 
        FROM `t_slider`";

        $stmt = $this->_pdo->prepare($query);
        $stmt->execute();//stmt es declaración
        $result = $stmt->fetchAll();

        return $result;
    }

    public function showSlider($id){

        $query =  "SELECT * 
        FROM `t_slider` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();//ejecuta la consulta
        $result = $stmt->fetch(PDO::FETCH_ASSOC); // array assoicativo cada campo de la base de datos aparecera un valor. $result['name'] y saldrá el nombre que queramos.

        return $result;
    }

    public function createSlider($slider, $img_url){

        $query = "insert into t_slider (title , description, img_url, link) 
        values (:title, :description, :img_url, :link)";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("title", $slider['title']);
        $stmt->bindParam("description", $slider['description']);
        $stmt->bindParam("img_url", $img_url);
        $stmt->bindParam("link", $slider['link']);
        $stmt->execute();

        $slider_id = $this->_pdo->lastInsertId();//hace la llamada a la base de datos, y dame el ultimo id que se ha añadido.

        return "Usuario añadido correctamente con el número de id " . $slider_id;
    }

    public function updateSlider($slider, $img_url){

        $query = "UPDATE t_slider set
            title = :title, 
            description = :description, 
            img_url = :img_url,  
            link = :link
            WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("title", $slider['title']);
        $stmt->bindParam("description", $slider['description']);
        $stmt->bindParam("img_url", $img_url);
        $stmt->bindParam("link", $slider['link']);
        $stmt->bindParam("id", $slider['id']);
        $stmt->execute();

        return "Usuario actualizado correctamente ".$slider['id'];
        
    }

    // public function updateSlider($slider, $img_url){

    //     $query = "UPDATE t_slider set
    //         title = $slider['title'], 
    //         description = $slider['description'],  
    //         link = $slider['link']
    //         WHERE id = $slider['id']";

    //     $stmt = $this->_pdo->prepare($query);
    //     $stmt->execute();

    //     $slider_id = $this->_pdo->lastInsertId();

    //     return "Usuario actualizado correctamente con $slider_id ";
        
    // }

    public function deleteSlider($id){
        $query =  "DELETE 
        FROM `t_slider` 
        WHERE id = :id";

        $stmt = $this->_pdo->prepare($query);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
         
        return 'Usuario con el id ' . $id . ' ha sido eliminado.' ;

    }

}

?>