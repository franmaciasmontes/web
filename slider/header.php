<!doctype html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<title>formulario</title>
	</head>
 <!--header-->
	<body>
		<div class="container-fluid">
			
			<div class="row header">
		
				<div class="logo col-8 col-sm-6 col-md-6 col-lg-3 col-xl-3">
					<img src="img/logo2.png"> 	 
				</div>

				<nav class="col-4 col-sm-4 col-md-6 col-lg-6 col-xl-6 navbar navbar-expand-lg navbar-light">
				  
					<div class="collapse navbar-collapse" id="navbarTogglerDemo02">
						<ul class="navbar-nav">
							<li class="nav-item">
								<a class="nav-link" href="index.php">inicio </a>
							</li>

							<li class="nav-item">
								<a class="nav-link" href="#"> cursos</a>
							</li>

							<li class="nav-item">
								<a class="nav-link" href="#">preinscripción </a>
							</li> 

							<li class="nav-item">
								<a class="nav-link" href="slider-form.php">slider</a>
							</li> 

							<li class="nav-item">
								<a class="nav-link" href="user-form.php">contacto</a>
                            </li>
                            <li class="nav-item">
								<a class="nav-link" href="blog-form.php">blog</a>
							</li>
						</ul>
					</div>
				</nav>				

			</div>