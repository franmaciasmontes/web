<?php

require_once('Slider.php');

class SliderController {
    
    protected $_slider;

    public function __construct(){
        $this->_slider = new Slider();
    }

    public function indexSlider(){
        return $this->_slider->indexSlider();
    }

    public function showSlider($id){

        return $this->_slider->showSlider($id);
    }
    
    public function createSlider($slider, $img_slider){
                
        $img_url = "practica_home/slider/img/slider/" . basename($img_slider['name']);
        move_uploaded_file ($img_slider["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $img_url);

        return $this->_slider->createSlider($slider, $img_url);
    }
    
    public function updateSlider($slider, $img_slider){

        $img_url = "practica_home/slider/img/slider/" . basename($img_slider['name']);
        move_uploaded_file ($img_slider["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . $img_url);
      
        return $this->_slider->updateSlider($slider, $img_url);
    }
    
    public function deleteSlider($id){

        return $this->_slider->deleteSlider($id);
    }

}    

?>