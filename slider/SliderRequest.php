<?php

    require_once('SliderController.php');
    require_once('core/Validation.php');

    /*$value='name' . $input_id = "name" $input_label = lo que se añade al cmapo nombre*/
    $validation = new Validation();// construir objeto en una variable, siempre arranca el constructor por si solo.
    $validation->imgEmpty($_FILES['img_url']['name'], 'img_url', 'img_url');
    $validation->fileType($_FILES['img_url'], 'img_url', array('gif','png', 'jpg', 'jpeg'));
    $validation->imgSize($_FILES['img_url']['size'], 'img_url', 'imagen');
    // $validation->fileFormat($_FILES['img']['tmp_name'],'img', 'imagen',array("image/gif","image/png"));

    if($validation ->getValidation()){ 
        $slider = new SliderController(); //$_POST, $_FILES['img']

        $response['_validation'] = $validation->getValidation();
        
        if(isset($_POST['id'])){
            $response['message'] = $slider->updateslider($_POST, $_FILES['img_url']); // aqui se pasa los datos de las base de datos del update. se necesita de un if o else para diferenciarlo
        }else{
            $response['message'] = $slider->createslider($_POST, $_FILES['img_url']); // aqui se pasa a la base de datos. llamas al método y el método te devuelve la info.
        }
        
        echo json_encode($response);
        
    } else{
    echo json_encode($validation->getErrors());
    }



?>